Certificate Revocation List (CRL):
        Version 2 (0x1)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: /CN=Autoridad de Certificacion Raiz del Estado Venezolano/C=VE/L=Caracas/ST=Distrito Capital/O=Sistema Nacional de Certificacion Electronica/OU=Superintendencia de Servicios de Certificacion Electronica/emailAddress=acraiz@suscerte.gob.ve
        Last Update: Nov  5 19:27:39 2013 GMT
        Next Update: May  4 19:27:39 2014 GMT
        CRL extensions:
            X509v3 Authority Key Identifier: 
                keyid:66:0D:9C:0C:AE:BA:D1:4A:43:03:EE:13:9B:6D:F1:D2:D4:72:D5:9A

            X509v3 Issuer Alternative Name: 
                DNS:suscerte.gob.ve, othername:<unsupported>
Revoked Certificates:
    Serial Number: 07
        Revocation Date: Mar 13 13:31:42 2012 GMT
    Serial Number: 04
        Revocation Date: Nov  5 19:15:08 2013 GMT
    Serial Number: 06
        Revocation Date: Jan  4 18:52:33 2011 GMT
    Serial Number: 09
        Revocation Date: Mar 13 13:33:51 2012 GMT
    Signature Algorithm: sha256WithRSAEncryption
        5b:d8:af:2e:a7:81:d9:4c:38:cf:68:9d:ed:fa:e4:fd:f1:38:
        08:8e:5e:89:1c:5a:25:3c:7a:12:65:4f:09:bd:40:2b:f2:96:
        13:bd:52:cf:e8:34:5b:00:c3:b2:84:2b:7e:9f:3d:5c:0a:85:
        21:6e:27:27:ea:93:8a:fc:64:54:14:c7:73:a1:3c:f7:9b:7a:
        2a:7e:20:ef:ba:8a:9c:11:72:31:d7:aa:29:ba:b9:d0:b6:32:
        c0:70:a1:2a:35:58:95:3e:bc:22:85:78:9c:ca:29:a6:a5:2f:
        ac:1d:47:a0:24:4f:12:8f:93:c3:ba:90:d2:02:16:5a:80:29:
        5e:95:81:25:7a:4b:c8:42:62:5d:37:5b:03:74:a7:42:b5:1d:
        a4:ea:a6:9f:0b:4b:99:2c:be:3a:04:58:ca:bf:75:03:15:84:
        b5:38:4f:18:6f:13:d7:6c:d4:75:05:72:fc:bd:85:6b:6a:af:
        b7:29:0c:bb:21:4c:16:29:22:9f:f5:3e:d8:da:9a:4c:eb:30:
        26:a3:41:63:17:40:7a:dc:e2:cb:03:44:4e:5d:7d:ed:1f:fd:
        0b:6f:31:9c:6e:f6:44:73:38:ed:f7:49:f5:ee:a5:8e:ea:f0:
        d6:8f:5b:c1:1a:31:de:e9:6d:78:4f:92:79:d5:e9:49:37:e6:
        30:d9:7f:50:28:03:a1:b4:ce:bf:84:65:32:0c:7e:f2:64:79:
        6e:47:a5:ce:ea:23:5a:97:bd:86:6e:26:8a:93:b4:7f:a6:06:
        8d:48:fc:b8:35:3d:55:36:66:8a:f0:9e:1f:e2:54:2c:7a:9b:
        cf:00:59:ae:3a:1c:68:40:cf:0d:9c:0c:f6:fc:03:82:9e:b9:
        4e:fe:b8:06:77:f7:12:5f:68:1d:80:9d:dd:94:43:03:15:93:
        fc:57:4e:fa:c6:5d:e5:5d:76:e3:5d:ad:7f:62:6f:7b:6b:47:
        ae:c1:ca:2f:26:6a:d1:c0:29:6c:ca:36:d4:d8:8e:ca:b3:a3:
        ee:5f:d8:51:6f:5b:9c:69:75:a4:87:dc:c3:bd:54:2e:4a:9d:
        a3:93:ef:35:96:08:00:2b:4c:27:1c:ea:b2:c6:da:8e:76:0e:
        17:cd:64:2d:f4:8d:4a:80:d6:89:8d:45:ab:51:e5:e3:bf:2f:
        91:81:ac:0f:8b:7a:98:dc:39:32:bc:c8:9a:ce:0c:53:bf:01:
        81:85:64:0f:8e:13:91:26:41:39:85:38:02:39:62:8e:da:1c:
        65:11:05:60:91:9a:e4:0b:b5:5b:c1:36:ef:b3:fe:e2:f6:65:
        44:65:47:b8:de:6d:5f:c1:f0:04:95:ef:a6:04:bb:f8:59:18:
        91:45:e3:d1:a5:f7:63:8d
