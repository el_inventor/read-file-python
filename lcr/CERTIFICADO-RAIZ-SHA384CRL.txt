Certificate Revocation List (CRL):
        Version 2 (0x1)
        Signature Algorithm: sha384WithRSAEncryption
        Issuer: /CN=Autoridad de Certificacion Raiz del Estado Venezolano/C=VE/L=Caracas/ST=Distrito Capital/O=Sistema Nacional de Certificacion Electronica/OU=Superintendencia de Servicios de Certificacion Electronica/emailAddress=acraiz@suscerte.gob.ve
        Last Update: May 22 15:31:35 2015 GMT
        Next Update: May 26 15:31:35 2015 GMT
        CRL extensions:
            X509v3 Authority Key Identifier: 
                keyid:AD:BB:22:1D:C6:E0:D2:01:A8:FD:76:50:52:93:ED:98:C1:4D:AE:D3

            X509v3 Issuer Alternative Name: 
                DNS:suscerte.gob.ve, othername:<unsupported>
Revoked Certificates:
    Serial Number: 0C
        Revocation Date: Jun 27 15:04:32 2011 GMT
    Serial Number: 12
        Revocation Date: Nov  5 19:16:33 2013 GMT
    Serial Number: 14
        Revocation Date: Nov 21 14:15:40 2014 GMT
    Serial Number: 10
        Revocation Date: Apr 29 19:26:41 2015 GMT
    Serial Number: 11
        Revocation Date: Apr 29 19:25:44 2015 GMT
    Signature Algorithm: sha384WithRSAEncryption
        83:55:83:cd:62:ed:28:94:95:76:a7:10:48:4d:31:05:7a:c8:
        4d:71:4c:ab:0b:84:c8:27:26:38:58:d8:07:11:99:e9:69:55:
        63:86:4d:47:93:13:be:a2:a8:de:ce:c0:ad:dc:b2:32:6c:31:
        46:47:88:11:1d:d3:ff:d3:2d:4a:a8:05:c8:22:e1:b4:4a:4b:
        d8:63:86:9a:f4:3e:c5:8a:51:9c:04:b5:64:a5:38:23:87:2e:
        ab:49:d8:4b:0c:5c:ab:dd:15:c8:14:cc:0a:d7:b7:41:79:1a:
        66:3b:d1:c9:18:85:f5:ed:bc:3d:4a:05:20:63:1e:68:07:5f:
        f7:e3:52:dc:2d:70:a5:e2:a9:23:5b:7f:89:77:b2:52:53:82:
        b9:98:0f:13:fd:1e:c4:47:66:c4:98:2c:3b:10:41:9b:22:da:
        ec:b6:41:12:80:a2:00:63:61:eb:42:ef:81:dc:2b:19:80:a6:
        1a:56:c1:ac:27:fa:33:51:53:4a:bb:4c:03:57:b2:29:1b:9e:
        e0:7e:94:3f:dd:7a:6a:b6:c4:fc:61:4f:c2:09:65:73:a4:d1:
        da:ee:00:40:1a:db:32:4d:ff:9a:1c:04:a6:f7:1c:77:85:bb:
        20:2a:5d:82:8c:42:e9:90:e2:eb:c9:6e:31:bc:91:12:ea:69:
        70:dd:13:73:55:ab:46:e8:86:de:01:92:33:3e:7e:6a:c8:51:
        67:e2:42:16:e2:7f:21:5e:43:de:55:c5:90:60:01:8d:8e:a6:
        37:38:92:ac:57:b5:86:79:60:dd:6c:bb:ea:e5:56:e2:11:6d:
        92:2b:f5:5e:ac:71:d1:0d:28:7d:de:71:41:16:93:86:28:0f:
        51:8e:14:e7:06:c0:e3:e7:0b:f9:dc:ea:e2:cd:88:43:b7:5c:
        db:ab:93:ad:41:57:b0:00:b3:63:be:e2:c6:7d:ae:7d:f1:bc:
        95:6e:96:cf:ad:48:80:c9:bb:0e:e4:90:9a:ef:96:d3:47:6e:
        4b:31:c7:e6:d2:0b:8c:7b:0f:03:e9:e2:bb:d2:0f:de:b6:2a:
        76:86:f0:05:b7:b6:84:69:5c:87:90:2e:4d:e8:ec:e7:b2:09:
        71:6b:8e:30:df:b7:31:8e:ec:61:1c:0b:4b:5f:d9:a1:53:49:
        a2:a3:91:d2:c5:a1:61:a6:3d:b3:f7:a6:3e:6d:c6:49:d9:74:
        67:a0:8c:c2:c2:be:e1:83:4b:1c:77:33:e8:20:e6:c1:15:c8:
        88:7f:bf:cf:83:b0:c4:2d:e5:8b:4c:ea:3b:7b:2d:b0:cd:e7:
        4a:31:31:6d:85:5b:9e:cf:dc:13:09:30:62:af:cf:aa:cc:5e:
        79:d4:25:a9:d0:12:0a:c8
