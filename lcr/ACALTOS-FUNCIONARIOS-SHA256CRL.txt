Certificate Revocation List (CRL):
        Version 2 (0x1)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: /CN=AC ALTOS FUNCIONARIOS/C=VE/L=Caracas/ST=Distrito Capital/O=Sistema Nacional de Certificacion Electronica/OU=Superintendencia de Servicios de Certificacion Electronica/emailAddress=acaltosfunc@suscerte.gob.ve
        Last Update: May 24 15:15:37 2013 GMT
        Next Update: May 25 15:15:37 2013 GMT
        CRL extensions:
            X509v3 Authority Key Identifier: 
                keyid:76:05:93:7A:C8:62:31:7F:55:2A:75:A2:6B:2B:F1:D6:CB:CD:1C:90

            X509v3 Issuer Alternative Name: 
                DNS:suscerte.gob.ve, othername:<unsupported>, othername:<unsupported>
Revoked Certificates:
    Serial Number: 44
        Revocation Date: Dec 19 14:48:32 2011 GMT
    Serial Number: 21
        Revocation Date: Jul 20 00:01:46 2011 GMT
    Serial Number: 20
        Revocation Date: Jul 20 00:02:37 2011 GMT
    Serial Number: 3C
        Revocation Date: Nov  7 16:41:38 2011 GMT
    Serial Number: 3B
        Revocation Date: Oct 31 19:34:47 2012 GMT
    Serial Number: 2E
        Revocation Date: May 24 14:53:23 2013 GMT
    Serial Number: 38
        Revocation Date: May 24 14:54:05 2013 GMT
    Serial Number: 39
        Revocation Date: May 24 14:55:12 2013 GMT
    Serial Number: 3A
        Revocation Date: May 24 14:55:34 2013 GMT
    Serial Number: 40
        Revocation Date: May 24 14:56:33 2013 GMT
    Serial Number: 3E
        Revocation Date: May 24 14:57:29 2013 GMT
    Serial Number: 16
        Revocation Date: Jul 19 05:55:35 2011 GMT
    Serial Number: 18
        Revocation Date: Jul 19 05:55:59 2011 GMT
    Serial Number: 15
        Revocation Date: Jul 19 05:57:11 2011 GMT
    Serial Number: 23
        Revocation Date: Apr 11 18:41:08 2012 GMT
    Serial Number: 1A
        Revocation Date: Oct 31 19:34:06 2012 GMT
    Serial Number: 27
        Revocation Date: Oct 31 19:34:25 2012 GMT
    Serial Number: 43
        Revocation Date: Oct 31 19:35:03 2012 GMT
    Serial Number: 1B
        Revocation Date: May 24 14:43:47 2013 GMT
    Serial Number: 19
        Revocation Date: May 24 14:49:09 2013 GMT
    Serial Number: 1D
        Revocation Date: May 24 14:49:37 2013 GMT
    Serial Number: 1E
        Revocation Date: May 24 14:50:08 2013 GMT
    Serial Number: 1F
        Revocation Date: May 24 14:50:29 2013 GMT
    Serial Number: 24
        Revocation Date: May 24 14:51:47 2013 GMT
    Serial Number: 29
        Revocation Date: May 24 14:52:16 2013 GMT
    Serial Number: 13
        Revocation Date: May 24 15:01:52 2013 GMT
    Serial Number: 12
        Revocation Date: May 24 15:02:59 2013 GMT
    Serial Number: 4A
        Revocation Date: Dec 19 14:49:33 2011 GMT
    Serial Number: 32
        Revocation Date: Dec 19 14:54:13 2011 GMT
    Serial Number: 37
        Revocation Date: May 24 14:50:41 2013 GMT
    Serial Number: 49
        Revocation Date: May 30 19:52:32 2012 GMT
    Serial Number: 36
        Revocation Date: Sep 12 18:49:52 2012 GMT
    Serial Number: 47
        Revocation Date: May 24 14:54:21 2013 GMT
    Serial Number: 33
        Revocation Date: May 24 14:54:44 2013 GMT
    Serial Number: 50
        Revocation Date: May 24 14:55:46 2013 GMT
    Serial Number: 34
        Revocation Date: May 24 14:59:06 2013 GMT
    Serial Number: 4D
        Revocation Date: May 24 14:59:49 2013 GMT
    Signature Algorithm: sha256WithRSAEncryption
        02:3c:a8:f8:26:9a:52:c4:e3:25:ab:e6:3d:c1:d3:ef:e4:1c:
        7a:60:b9:85:69:b5:42:93:e3:9a:6e:09:c0:3e:99:f8:7a:00:
        c6:6f:8a:ba:97:12:02:f1:1a:be:61:9a:57:a8:df:4f:75:a6:
        b1:0a:82:51:ef:72:4f:c1:ec:53:2e:4e:71:fc:97:1a:15:90:
        3a:b1:5e:3c:eb:14:e2:58:22:2d:08:15:24:f4:77:6c:5b:f5:
        31:dc:b8:17:61:fc:d6:21:f8:45:49:d2:e7:79:77:88:b1:54:
        ee:de:b4:07:cf:e8:9f:09:c4:b2:f7:0b:1f:f0:be:3a:66:ce:
        9e:2c:e6:3b:8d:a8:d8:31:24:91:1d:22:29:fc:61:59:1d:63:
        2d:6c:09:4a:91:24:8e:e0:5c:ff:82:42:e8:9b:87:d4:09:9f:
        4f:9c:c5:a1:6c:2b:1f:10:07:c1:b1:aa:3b:91:76:67:6d:8c:
        c9:e6:c3:95:6a:42:7c:9b:a8:ad:c8:82:96:23:f4:85:f3:75:
        e4:8c:6b:34:4c:14:25:01:2d:38:a0:28:59:e1:e1:b7:50:26:
        dc:58:f8:aa:d3:ae:a2:4e:d8:b3:7d:49:0d:aa:cd:f4:10:e6:
        36:8d:46:e8:94:69:fb:4b:aa:d0:ed:70:5b:b9:c5:2e:58:65:
        50:b7:17:7b:de:1e:96:1f:16:7c:bc:88:5c:81:5c:d5:0b:94:
        40:03:81:08:10:62:70:c9:b1:c6:39:32:d6:5b:4d:c2:74:82:
        b4:f4:20:d3:a7:30:ba:0b:3e:3b:61:98:f3:f0:85:4c:b5:b3:
        31:e6:57:0c:14:1c:e6:f1:9b:e7:cb:5c:9c:3f:32:a9:9a:43:
        a5:5d:e3:cf:18:f2:2f:d9:97:cc:82:2e:d8:77:56:c0:3c:af:
        ce:a5:f2:02:5a:45:82:cd:c8:22:8f:7c:dc:82:2e:b4:eb:fc:
        41:6c:ea:b5:b8:7b:8c:6a:d0:94:56:96:bd:d1:e2:03:32:d3:
        8a:6c:06:77:c0:1c:7b:43:27:07:2f:a2:36:23:e6:95:a4:68:
        b6:b2:50:29:bb:48:eb:5b:93:3f:c9:f5:b5:7e:39:a2:da:b4:
        2f:16:30:04:32:1b:a6:b6:33:79:bb:06:5a:f6:d0:07:74:ac:
        2e:ea:2e:7c:bc:7f:ee:48:45:83:fa:9f:9f:05:35:b8:cd:9a:
        fe:6c:01:82:72:36:e7:6e:c0:ea:27:d0:e6:27:5b:10:e4:25:
        a7:73:34:fc:8d:3c:ec:07:9a:37:06:32:d6:68:99:ef:06:d0:
        b3:96:6e:7b:56:d3:ab:e7:dc:4b:9b:28:c4:42:d1:27:0b:bf:
        dd:ac:f9:0d:fe:44:5e:7d
