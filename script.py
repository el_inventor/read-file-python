#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import datetime


def check_algoritmo(datafile):
    #datafile = file(archivo)

    for line in datafile:
        if line.__contains__("Signature Algorithm: "):
            splitalgoritmo = line.split("Signature Algorithm: ")
            campo = splitalgoritmo[1]
            if campo.__contains__("WithRSAEncryption"):
                spliteado = campo.split("WithRSAEncryption")
                algoritmo = spliteado[0]
                #print algoritmo
                break
    return algoritmo

def formatear_fecha_gtm(cadena):
    '''
    Función que retorna una fecha de forma 
    'Mar 25 14:55:00 2011' en '2011-03-25 14:55:00'
    '''
    fecha = datetime.datetime.strptime(cadena, '%b %d %H:%M:%S %Y')

    return fecha


def check_fecha(datafile, inicio, fin):
    for line in datafile:

        # Fecha inicio              
        if line.__contains__(inicio):
            splitbefore = line.split(inicio)
            campo2 = splitbefore[1]
            if campo2.__contains__(" GMT"):
                spliteado = campo2.split(" GMT")
                fecha_ini = spliteado[0]
                #print fecha_ini
                print formatear_fecha_gtm(fecha_ini)

        # Fecha fin         
        if line.__contains__(fin):
            splitbefore = line.split(fin)
            campo3 = splitbefore[1]
            if campo3.__contains__(" GMT"):
                spliteado = campo3.split(" GMT")
                fecha_fin = spliteado[0]
                #print fecha_fin
                print formatear_fecha_gtm(fecha_fin)
            break


def consulta():
    archivo = raw_input("Ingrese la ruta del archivo: ")
    datafile = file(archivo)    

    print check_algoritmo(datafile)
    #check_fecha(datafile, "Not Before: ", "Not After : ")
    check_fecha(datafile, "Last Update: ","Next Update: ")


consulta()    
